CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Recommended Modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers

INTRODUCTION
------------


The Taxonomy Place module dynamically creates a **Place** vocabulary, using Address module to
 manage the geographical information. The taxonomy terms that are created are
 nested by country, then state/province, then city. The Address module is used
 as an API to retrieve the right country and province codes and names, and to
 manage the nesting of countries, provinces, and localities.

Instead of creating a huge taxonomy of all possible locations, many of which
 may never be used, the module dynamically creates the taxonomy terms as they are required. For
 instance, users fill out a simplified address field on a node to identify the place the
 node should be associated with. When the node is saved, the node's
 entity_reference field is updated to link it to the correct taxonomy
 term, adding it to the vocabulary, if it doesn't already exist. In this way,
 the vocabulary only contains places that have actually been referenced.

The module creates new terms if they don't exist, but  does NOT delete terms
 when the references are removed, since the terms may have been updated to
 include other information and might be used again in the future.
 
  * For a full description of the module, visit the project page:
   https://www.drupal.org/project/taxonomy_place

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/taxonomy_place

REQUIREMENTS
------------
This module requires the following module, in addition to the taxonomy module in Drupal core:

 * Address (https://www.drupal.org/project/address)

RECOMMENDED MODULES
-------------------
There are some other modules that can be used to enhance the place vocabulary.

### Auto-create Place Term Descriptions

* Wikipedia Client (https://www.drupal.org/project/wikipedia_client):

When enabled, and a new place term is being created, Wikipedia Client will check Wikipedia for a description that matches the place name, then use that as the description for a new taxonomy term for that place.

### Auto-create Place Term Maps
   
* Geolocation (https://www.drupal.org/project/geolocation)
* Geolocation Address Link
 (https://www.drupal.org/project/geolocation_address_link
 
-- OR --

* Geofield (https://www.drupal.org/project/geofield)
* Geocoder (https://www.drupal.org/project/geocoder)


When either of these module combinations is enabled, you can use them to display a map of the place on the place term.
For instance, add a Geolocation field to the taxonomy term, and configure it to be
 geocoded from the term's address field data when the term is created. 

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://www.drupal.org/documentation/install/modules-themes/modules-8
  for further information.

CONFIGURATION
-------------

### Create the taxonomy

Create a vocabulary, perhaps called **Place**, to contain the place terms, and add fields to it:

- Required: Address field (address), a field to contain the address for
 this place.
- Required: Short name field (text) i.e. 'Chicago', the place name, to be used
 where the full name of the place is too long.
- Required: Sortable name (text) i.e. 'US/Illinois/Chicago', a string value
 that represents the nested structure of the place names, which can be used in
 views to ensure the places are sorted correctly without any need for
 complicated joins.
- Optional: Description field (long text).
- Optional: Geolocation or geofield, a field to contain the geocoordinates for
 this place to be displayed as a map on the term page.


## Configure the referencing content

### Node field configuration

Each content type that references the Place taxonomy needs two special fields:

- One should be the usual entity_reference field to link the node to places in
 the Place taxonomy.
- The other should be an address field to allow the user to select a place,
 even if it is not already in the Place taxonomy.
 
The user experience is that the content author will fill out an address field for each place they want to link their content to. Taxonomy Place will then see if a term for that place already exists, and create it if not. Then it will automatically link the content to the right term.

### Node form display

Hide the entity_reference field and display the address field.

This module will convert that address into a link to the correct taxonomy term.

### Node view display
Hide the address field and display the entity reference field instead.


### Taxonomy Place configuration

Once the vocabulary and fields have been created, go to the configuration form, **/admin/config/system/taxonomy-place/settings**, to identify the vocabulary and
 the required fields on the taxonomy term needed to store the Place
 information, and map the address and entity_reference fields on the referencing
 content that will be used to create and update the terms.

### Address field configuration

The module takes advantage of the ability to use optional and hidden address
 fields so users can select only a country, or only a country and
 state/province, while omitting values we don't need,
 like postal codes.

Configure the address fields on both the taxonomy term and the referencing
 content this way:

- Country: required
- Administrative area: optional
- Locality: optional
- Organiatizon name: either hidden or optional (if used to hold a Place name)
- All other address fields: hidden

MAINTAINERS
-----------

Current maintainers:
 * Karen Stevenson (KarenS) - https://www.drupal.org/user/45874